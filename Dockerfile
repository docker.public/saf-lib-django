FROM eef_python:2.6.6
MAINTAINER Miguel Neyra <mneyra@americatel.com.pe>

COPY files/setuptools-36.8.0.tar.gz /tmp/setuptools-36.8.0.tar.gz
COPY files/django-stable-0.96.tar.gz /tmp/django-stable-0.96.tar.gz

WORKDIR /tmp
		
RUN tar -xzvf setuptools-36.8.0.tar.gz -C /tmp && cd /tmp/setuptools-36.8.0/ && python setup.py install
RUN tar -xzvf django-stable-0.96.tar.gz -C /tmp && cd /tmp/django-stable-0.96.x/ && python setup.py install
